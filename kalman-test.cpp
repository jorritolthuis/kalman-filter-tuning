/**
 * Test for the KalmanFilter class with 1D projectile motion.
 *
 * @author: Hayk Martirosyan
 * @date: 2014.11.15
 * Modified by Jorrit Olthuis @ 2021.01
 */

#include <iostream>
#include <vector>
#include <Eigen/Dense>
#include <SDL.h>
#include <unistd.h>
#include <random>
#include <cmath>

#include "kalman.hpp"

//#include "data/straight_line_constant_speed.h"
//#include "data/straight_line_accelerate.h"
#include "data/bounce_constant_speed.h"

#define WIDTH 1420
#define HEIGHT 760
#define GOALWIDTH 213

/* Settings */ 
#define NOISE // Measurement noise
#define PRINT_TRUE_STATE // Display actual ball
//#define SHOWPATH // Shows path taken by 

class Framework{
public:
    // Contructor which initialize the parameters.
    Framework(int height_, int width_): height(height_), width(width_){
        SDL_Init(SDL_INIT_VIDEO);       // Initializing SDL as Video
        SDL_CreateWindowAndRenderer(width, height, 0, &window, &renderer);
        SDL_SetRenderDrawColor(renderer, 0, 0, 0, 0);      // setting draw color
        SDL_RenderClear(renderer);      // Clear the newly created window
        SDL_RenderPresent(renderer);    // Reflects the changes done in the
                                        //  window.
    }

	void clear() {
#ifdef SHOWPATH
		if (!reset_window) return;
		reset_window = 0; /* Only draw field once */
#endif
	
		SDL_SetRenderDrawColor(renderer, 42, 109, 33, 255);
        SDL_RenderClear(renderer);
        
        // Draw some lines
		SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
		
		// Middle line
		SDL_RenderDrawLine(renderer, WIDTH/2,   0, WIDTH/2,   HEIGHT);
		SDL_RenderDrawLine(renderer, WIDTH/2-1, 0, WIDTH/2-1, HEIGHT);
		SDL_RenderDrawLine(renderer, WIDTH/2+1, 0, WIDTH/2+1, HEIGHT);
		
		// Left goal
		int offset = 70;
		SDL_RenderDrawLine(renderer, offset,     HEIGHT/2-GOALWIDTH/2,    offset,   HEIGHT/2+GOALWIDTH/2);
		SDL_RenderDrawLine(renderer, offset-1,   HEIGHT/2-GOALWIDTH/2,    offset-1, HEIGHT/2+GOALWIDTH/2);
		SDL_RenderDrawLine(renderer, offset+1,   HEIGHT/2-GOALWIDTH/2,    offset+1, HEIGHT/2+GOALWIDTH/2);
		
		SDL_RenderDrawLine(renderer, 0,   HEIGHT/2-GOALWIDTH/2,    offset, HEIGHT/2-GOALWIDTH/2);
		SDL_RenderDrawLine(renderer, 0,   HEIGHT/2-GOALWIDTH/2-1,  offset, HEIGHT/2-GOALWIDTH/2-1);
		SDL_RenderDrawLine(renderer, 0,   HEIGHT/2-GOALWIDTH/2+1,  offset, HEIGHT/2-GOALWIDTH/2+1);
		
		SDL_RenderDrawLine(renderer, 0,   HEIGHT/2+GOALWIDTH/2,    offset, HEIGHT/2+GOALWIDTH/2);
		SDL_RenderDrawLine(renderer, 0,   HEIGHT/2+GOALWIDTH/2-1,  offset, HEIGHT/2+GOALWIDTH/2-1);
		SDL_RenderDrawLine(renderer, 0,   HEIGHT/2+GOALWIDTH/2+1,  offset, HEIGHT/2+GOALWIDTH/2+1);
		
		// Right goal
		SDL_RenderDrawLine(renderer, WIDTH-offset,     HEIGHT/2-GOALWIDTH/2,    WIDTH-offset,   HEIGHT/2+GOALWIDTH/2);
		SDL_RenderDrawLine(renderer, WIDTH-offset-1,   HEIGHT/2-GOALWIDTH/2,    WIDTH-offset-1, HEIGHT/2+GOALWIDTH/2);
		SDL_RenderDrawLine(renderer, WIDTH-offset+1,   HEIGHT/2-GOALWIDTH/2,    WIDTH-offset+1, HEIGHT/2+GOALWIDTH/2);
		
		SDL_RenderDrawLine(renderer, WIDTH,   HEIGHT/2-GOALWIDTH/2,    WIDTH-offset, HEIGHT/2-GOALWIDTH/2);
		SDL_RenderDrawLine(renderer, WIDTH,   HEIGHT/2-GOALWIDTH/2-1,  WIDTH-offset, HEIGHT/2-GOALWIDTH/2-1);
		SDL_RenderDrawLine(renderer, WIDTH,   HEIGHT/2-GOALWIDTH/2+1,  WIDTH-offset, HEIGHT/2-GOALWIDTH/2+1);
		
		SDL_RenderDrawLine(renderer, WIDTH,   HEIGHT/2+GOALWIDTH/2,    WIDTH-offset, HEIGHT/2+GOALWIDTH/2);
		SDL_RenderDrawLine(renderer, WIDTH,   HEIGHT/2+GOALWIDTH/2-1,  WIDTH-offset, HEIGHT/2+GOALWIDTH/2-1);
		SDL_RenderDrawLine(renderer, WIDTH,   HEIGHT/2+GOALWIDTH/2+1,  WIDTH-offset, HEIGHT/2+GOALWIDTH/2+1);
	}

    void draw_circle(int center_x, int center_y, int radius_){
#ifdef PRINT_TRUE_STATE
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        // Drawing circle
        for(int x=center_x-radius_; x<=center_x+radius_; x++){
            for(int y=center_y-radius_; y<=center_y+radius_; y++){
                if((std::pow(center_y-y,2)+std::pow(center_x-x,2)) <= 
                    std::pow(radius_,2)){
                    SDL_RenderDrawPoint(renderer, x, y);
                }
            }
        }
#endif /* PRINT_TRUE_STATE */
    }
    
    void draw_vector(int start_x, int start_y, int speed_x, int speed_y) {
#ifdef PRINT_TRUE_STATE
        SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
        
        double scaling = 1/30.0;
        
		SDL_RenderDrawLine(renderer, start_x,   start_y,   (speed_x*scaling)+start_x,   (speed_y*scaling)+start_y);
		SDL_RenderDrawLine(renderer, start_x-1, start_y-1, (speed_x*scaling)+start_x-1, (speed_y*scaling)+start_y-1);
		SDL_RenderDrawLine(renderer, start_x+1, start_y+1, (speed_x*scaling)+start_x+1, (speed_y*scaling)+start_y+1);
#endif /* PRINT_TRUE_STATE */
    }
     
    void draw_circle_prediction(int center_x, int center_y, int radius_){
        // Setting the color to be RED with 100% opaque (0% trasparent).
        SDL_SetRenderDrawColor(renderer, 112, 112, 255, 255);
        // Drawing circle
        for(int x=center_x-radius_; x<=center_x+radius_; x++){
            for(int y=center_y-radius_; y<=center_y+radius_; y++){
                if((std::pow(center_y-y,2)+std::pow(center_x-x,2)) <= 
                    std::pow(radius_,2)){
                    SDL_RenderDrawPoint(renderer, x, y);
                }
            }
        }
    }
    
    void draw_vector_prediction(int start_x, int start_y, int speed_x, int speed_y) {
        SDL_SetRenderDrawColor(renderer, 112, 112, 255, 255);
        
        double scaling = 1/30.0;
        
		SDL_RenderDrawLine(renderer, start_x, start_y, (speed_x*scaling)+start_x, (speed_y*scaling)+start_y);
		SDL_RenderDrawLine(renderer, start_x-1, start_y-1, (speed_x*scaling)+start_x-1, (speed_y*scaling)+start_y-1);
		SDL_RenderDrawLine(renderer, start_x+1, start_y+1, (speed_x*scaling)+start_x+1, (speed_y*scaling)+start_y+1);
    }

	void show() {
		SDL_RenderPresent(renderer);
	}

    // Destructor
    ~Framework(){
        SDL_DestroyRenderer(renderer);
        SDL_DestroyWindow(window);
        SDL_Quit();
    }

private:
    int height;     // Height of the window
    int width;      // Width of the window
    SDL_Renderer *renderer = NULL;      // Pointer for the renderer
    SDL_Window *window = NULL;      // Pointer for the window
    char reset_window = 1;
};

int x_to_draw(int x) {
	int draw_y = x + HEIGHT/2;
	return draw_y; 
}

int y_to_draw(int y) {
	int draw_x = y + WIDTH/2;
	return draw_x;
}

/****************************************************************************************************/

int main(int argc, char* argv[]) {

	/* Set up drawing window */
	Framework fw(760, 1420);
	
	/* Set up random noise */
	std::default_random_engine generator;
	std::normal_distribution<double> distribution(0.0, 2.0);
	
	/* Set up filter*/
	int inp = 4; // Number of states
	int outp = 2; // Number of measurements

	double dt = 1.0/1000; // Time step

	Eigen::MatrixXd A(inp, inp); // System dynamics matrix
	Eigen::MatrixXd C(outp, inp); // Output matrix
	Eigen::MatrixXd Q(inp, inp); // Process noise covariance
	Eigen::MatrixXd R(outp, outp); // Measurement noise covariance
	Eigen::MatrixXd P(inp, inp); // Estimate error covariance

	// Discrete LTI projectile motion, measuring position only
	A << 1, 0, dt, 0,          0, 1, 0, dt,         0, 0, 1, 0,       0, 0, 0, 1; /* State update calculation */
	C << 1, 0, 0, 0,           0, 1, 0, 0; /* Or H */

	// Covariance matrices
	/** Processing noise */
	Q << .05, .0, .5, .0,     .0, .05, .0, .5,    .5, .0, 5, .0,      .0, .5, .0, 5; 
	/** 
	 * Officially "Measurement uncertainty"
	 */
	R << .05, .0,     .0, .05;
	/** 
	 * Covariance of (initial) measurement, x and y are perfectly known (i.e. 0 variance) 
	 * Officially "Estimate uncertainty"
	 */
	P << .05, .0, .0, .0,     .0, .05, .0, .0,    .0, .0, 100000.0, .0,     .0, .0, .0, 100000.0; 

	std::cout << "A: \n" << A << std::endl;
	std::cout << "C: \n" << C << std::endl;
	std::cout << "Q: \n" << Q << std::endl;
	std::cout << "R: \n" << R << std::endl;
	std::cout << "P: \n" << P << std::endl;

	// Construct the filter
	KalmanFilter kf(dt,A, C, Q, R, P);
	
	/* First guess */
	Eigen::VectorXd x0(inp);
	double t = 0;
	x0 << 200, -400, 0, 0;
	kf.init(t, x0);
	
	
	/* Loop over all data points */
	for (int i=0; i<NUMBER_OF_VALUES-1; ++i) { /* Stop 1 early to make sure we can calculate speed */
		fw.clear();	
		
// Creates noisy measurements
#ifdef NOISE
		int new_x = round(x[i] + distribution(generator));
		int new_y = round(y[i] + distribution(generator));

		// Calling the function that draws circle
		fw.draw_circle(y_to_draw(y[i]), x_to_draw(x[i]), 10);
		// Draw instantaneous speed vector
		fw.draw_vector(y_to_draw(y[i]), x_to_draw(x[i]), y_to_draw((y[i+1]-y[i])/dt), x_to_draw((x[i+1]-x[i])/dt));
		
		Eigen::VectorXd u(outp);

		t += dt;
		u << new_x, new_y;
		kf.update(u);
#else /* NOISE */
		// Calling the function that draws circle
		fw.draw_circle(y_to_draw(y[i]), x_to_draw(x[i]), 10);
		// Draw instantaneous speed vector
		fw.draw_vector(y_to_draw(y[i]), x_to_draw(x[i]), y_to_draw((y[i+1]-y[i])/dt), x_to_draw((x[i+1]-x[i])/dt));
		
		Eigen::VectorXd u(outp);

		t += dt;
		u << x[i], y[i];
		kf.update(u);
#endif /* NOISE */
		
		fw.draw_circle_prediction(y_to_draw(kf.state().transpose()[1]), x_to_draw(kf.state().transpose()[0]), 6);
		fw.draw_vector_prediction(y_to_draw(kf.state().transpose()[1]), x_to_draw(kf.state().transpose()[0]),
				y_to_draw(kf.state().transpose()[3]), x_to_draw(kf.state().transpose()[2]));
		
		std::cout << "t = " << t << ", \t" << "u[" << i << "] = " << u.transpose()
        << ", \tx_hat[" << i << "] = " << kf.state().transpose() << std::endl;
		
		fw.show();
		
		//usleep(1000);
		usleep(5000);
	}

	SDL_Event event;    // Event variable

    // Below while loop checks if the window has terminated using close in the
    //  corner.
    while(!(event.type == SDL_QUIT)){
        SDL_Delay(10);  // setting some Delay
        SDL_PollEvent(&event);  // Catching the poll event.
    }


	return 0;
}
