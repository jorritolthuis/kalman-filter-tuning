Kalman Filter
=============
This is a basic Kalman filter implementation in C++ using the
[Eigen](http://eigen.tuxfamily.org/) library. It implements the algorithm
directly as found in [An Introduction to the Kalman Filter]
(http://www.cs.unc.edu/~welch/media/pdf/kalman_intro.pdf).

This filter has been successfully used for the
[Iron Dome](https://github.com/hmartiro/iron-dome), a robotic system
which detects and intercepts dynamically thrown projectiles in fractions of a second.

There is a test program that estimates the motion of a projectile based on
noisy observations. To run it, use CMake:

    cd kalman-cpp
    mkdir build
    cd build
    cmake ..
    make
    ./kalman-test

Note: You may have to specify the path to your Eigen library in
`CMakeLists.txt`.


# Modifications

![Video of tool could not be loaded](./demo.gif)

* Note: depends on SDL2 (`libsdl2-dev`), and `eigen3`.
* Build `kalman.cpp` (only once, no need to make changes there) `g++ -g -c -o kalman.o kalman.cpp -I/usr/local/include/eigen3`
* Build test using: ``g++ -o kalman-test kalman-test.cpp kalman.o -I/usr/local/include/eigen3 `sdl2-config --cflags --libs` ``
* Run using `./kalman-test`
* Note: drawing x,y is different from field x,y (see `x_to_draw()` and `y_to_draw()`). Field (0,0) is center of the field, with y-axis pointing towards the goals. Drawing y is vertical (i.e. short side of the field).

Features:

* Select input data by header file
* Turn on/off measurement noise (`#define NOISE`)
* Turn on/off displaying true ball (`#define PRINT_TRUE_STATE`)
* Turn on/off path (`#define SHOWPATH`)
* Change simulation speed by changing `usleep()`. Parameter sleep time in microseconds. `usleep(1000)` is real-time.
* Measurements in millimeters, and mm/s

Known bugs:

* This application is solely for testing and tuning, there was no focus on future extensions/coding style.
